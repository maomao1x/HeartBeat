package com.andaily.hb.service.impl;

import com.andaily.hb.domain.dto.log.FrequencyMonitorLogListDto;
import com.andaily.hb.domain.dto.log.ReminderLogListDto;
import com.andaily.hb.service.LogService;
import com.andaily.hb.service.operation.FrequencyMonitorLogListDtoLoader;
import com.andaily.hb.service.operation.ReminderLogListDtoLoader;
import com.andaily.hb.service.operation.job.AutoMonitorLogCleaner;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 15-2-13
 *
 * @author Shengzhao Li
 */
@Service("logService")
public class LogServiceImpl implements LogService {


    @Transactional(readOnly = true)
    @Override
    public FrequencyMonitorLogListDto loadFrequencyMonitorLogListDto(FrequencyMonitorLogListDto listDto) {
        FrequencyMonitorLogListDtoLoader dtoLoader = new FrequencyMonitorLogListDtoLoader(listDto);
        return dtoLoader.load();
    }

    @Transactional(readOnly = true)
    @Override
    public ReminderLogListDto loadReminderLogListDto(ReminderLogListDto listDto) {
        ReminderLogListDtoLoader dtoLoader = new ReminderLogListDtoLoader(listDto);
        return dtoLoader.load();
    }

    @Transactional
    @Override
    public long executeAutoCleanMonitorLogs() {
        AutoMonitorLogCleaner autoMonitorLogCleaner = new AutoMonitorLogCleaner();
        return autoMonitorLogCleaner.clean();
    }
}
