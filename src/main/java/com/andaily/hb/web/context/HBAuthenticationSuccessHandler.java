package com.andaily.hb.web.context;

import com.andaily.hb.infrastructure.ThreadLocalHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 2017/7/9
 *
 * @author Shengzhao Li
 */
@Component
public class HBAuthenticationSuccessHandler implements AuthenticationSuccessHandler {


    private static final Logger LOG = LoggerFactory.getLogger(HBAuthenticationSuccessHandler.class);


    private String successRedirectUrl = "/instance/list.hb";


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        LOG.debug("{}|Login Successful, IP: {}", authentication.getPrincipal(), ThreadLocalHolder.clientIp());
        response.sendRedirect(request.getContextPath() + this.successRedirectUrl);
    }


    public void setSuccessRedirectUrl(String successRedirectUrl) {
        this.successRedirectUrl = successRedirectUrl;
    }
}
